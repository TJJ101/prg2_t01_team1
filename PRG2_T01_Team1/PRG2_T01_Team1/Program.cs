﻿//============================================================
// Student Number : S0194152D, S10205563J
// Student Name : Tan Jun Jie, Amos Yeong
// Module Group : T01
//============================================================

using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Globalization;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json;

namespace PRG2_T01_Team1
{
    class Program
    {
        static void Main(string[] args)
        {
            //List of everything
            List<Person> pList = new List<Person>();
            List<Visitor> vList = new List<Visitor>();
            List<Resident> rList = new List<Resident>();
            List<TraceTogetherToken> tttList = new List<TraceTogetherToken>();
            List<BusinessLocation> blList = new List<BusinessLocation>();
            List<SHNFacility> shnList = new List<SHNFacility>();
            List<SafeEntry> seList = new List<SafeEntry>();
            List<string> collectionList = new List<string>();

            //Loading all the data
            LoadSHNData(shnList);
            LoadPersonData(pList, rList, vList, tttList, shnList);
            LoadBusinessLocationData(blList);
            CultureInfo culture = new CultureInfo("en-SG");
            while (true)
            {
                DisplayMenu();
                InitCCList(collectionList);
                Console.WriteLine("-----------------------------");
                Console.Write("Enter Options: ");
                string option = Console.ReadLine();
                Console.WriteLine();
                if (option == "1")
                {
                    DisplayVisitor(vList);
                    Console.WriteLine();
                }
                else if (option == "2")//prompt user to input and print user details
                {
                    while (true)
                    {
                        DisplayPerson(pList);
                        Console.WriteLine();
                        Console.Write("Enter Name to Search (Or 0 to Exit): ");
                        string name = Console.ReadLine();
                        Console.WriteLine();
                        if (name == "0") // To enable users to exit to main menu whenever they want
                        {
                            Console.WriteLine();
                            break;
                        }
                        Person p1 = SearchForPerson(pList, name);
                        if(p1 != null)
                        {
                            if(SearchResident(rList, p1.Name) != null)
                            {
                                Resident r1 = SearchResident(rList, p1.Name);
                                if (r1.TravelEntryList.Count != 0)
                                {
                                    if(p1.SafeEntryList.Count != 0)
                                    {
                                        if (r1.Token != null)
                                        {
                                            Console.WriteLine("{0, -7} {1, -15} {2, -25} {3, -13} {4, -15} {5, -22} {6, -22} {7, -23} {8}",
                                                "Name", "Address", "Last Left Country", "Last Country", "Entry Mode", "Entry Date", "Check In", "Check Out", "Location");
                                            Console.WriteLine("----------------------------------------------------------------------------------------------------------------------------------------------------------------------");
                                            Console.WriteLine("{0, -7} {1, -15} {2, -25} {3, -13} {4, -15} {5, -22} {6, -22} {7, -23} {8}",
                                                r1.Name, r1.Address, r1.LastLeftCountry, r1.TravelEntryList[0].LastCountryOfEmbarkation, r1.TravelEntryList[0].EntryMode, r1.TravelEntryList[0].EntryDate, p1.SafeEntryList[0].CheckIn, p1.SafeEntryList[0].CheckOut, p1.SafeEntryList[0].Location.BusinessName, r1.Token.SerialNo, r1.Token.CollectionLocation, r1.Token.ExpiryDate);
                                            Console.WriteLine();
                                            Console.WriteLine("{0, -10} {1, -22} {2}",
                                                "Serial No", "Collection Location", "Expiry Date");
                                            Console.WriteLine("----------------------------------------------------------------------------------------------------------------------------------------------------------------------");
                                            Console.WriteLine("{0, -10} {1, -22} {2}",
                                                 r1.Token.SerialNo, r1.Token.CollectionLocation, r1.Token.ExpiryDate);
                                            Console.WriteLine();
                                            break;
                                        }
                                        else
                                        {
                                            if(r1.SafeEntryList[0].CheckOut == DateTime.MinValue)
                                            {
                                                Console.WriteLine("{0, -7} {1, -15} {2, -25} {3, -13} {4, -15} {5, -22} {6, -22} {7, -15} {8}",
                                                    "Name", "Address", "Last Left Country", "Last Country", "Entry Mode", "Entry Date", "Check In", "Check Out", "Location");
                                                Console.WriteLine("----------------------------------------------------------------------------------------------------------------------------------------------------------------------");
                                                Console.WriteLine("{0, -7} {1, -15} {2, -25} {3, -13} {4, -15} {5, -22} {6, -22} {7, -15} {8}",
                                                    r1.Name, r1.Address, r1.LastLeftCountry, r1.TravelEntryList[0].LastCountryOfEmbarkation, r1.TravelEntryList[0].EntryMode, r1.TravelEntryList[0].EntryDate, p1.SafeEntryList[0].CheckIn, "-", p1.SafeEntryList[0].Location.BusinessName);
                                                Console.WriteLine("");
                                                break;
                                            }
                                            else
                                            {
                                                Console.WriteLine("{0, -7} {1, -15} {2, -25} {3, -13} {4, -15} {5, -22} {6, -22} {7, -22} {8}",
                                                    "Name", "Address", "Last Left Country", "Last Country", "Entry Mode", "Entry Date", "Check In", "Check Out", "Location");
                                                Console.WriteLine("----------------------------------------------------------------------------------------------------------------------------------------------------------------------");
                                                Console.WriteLine("{0, -5} {1, -15} {2, -25} {3, -13} {4, -15} {5, -22} {6, -22} {7, -22} {8}",
                                                    r1.Name, r1.Address, r1.LastLeftCountry, r1.TravelEntryList[0].LastCountryOfEmbarkation, r1.TravelEntryList[0].EntryMode, r1.TravelEntryList[0].EntryDate, p1.SafeEntryList[0].CheckIn, p1.SafeEntryList[0].CheckOut, p1.SafeEntryList[0].Location.BusinessName);
                                                Console.WriteLine();
                                                break;
                                            }

                                        }
                                    }
                                    else if(r1.Token != null)
                                    {
                                        Console.WriteLine("{0, -7} {1, -15} {2, -25} {3, -13} {4, -15} {5, -22} {6, -12} {7, -20} {8}",
                                            "Name", "Address", "Last Left Country", "Last Country", "Entry Mode", "Entry Date", "Serial No", "Collection Location", "Expiry Date");
                                        Console.WriteLine("----------------------------------------------------------------------------------------------------------------------------------------------------------------------");
                                        Console.WriteLine("{0, -7} {1, -15} {2, -25} {3, -13} {4, -15} {5, -22} {6, -12} {7, -20} {8}",
                                            r1.Name, r1.Address, r1.LastLeftCountry, r1.TravelEntryList[0].LastCountryOfEmbarkation, r1.TravelEntryList[0].EntryMode, r1.TravelEntryList[0].EntryDate, r1.Token.SerialNo, r1.Token.CollectionLocation, r1.Token.ExpiryDate);
                                        Console.WriteLine();
                                        break;
                                    }
                                    else
                                    {
                                        Console.WriteLine("{0, -7} {1, -15} {2, -25} {3, -13} {4, -15} {5}",
                                            "Name", "Address", "Last Left Country", "Last Country", "Entry Mode", "Entry Date");
                                        Console.WriteLine("-------------------------------------------------------------------------------------------------------------------------");
                                        Console.WriteLine("{0, -7} {1, -15} {2, -25} {3, -13} {4, -15} {5}",
                                            r1.Name, r1.Address, r1.LastLeftCountry, r1.TravelEntryList[0].LastCountryOfEmbarkation, r1.TravelEntryList[0].EntryMode, r1.TravelEntryList[0].EntryDate);
                                        Console.WriteLine();
                                        break;
                                    }
                                }
                                else if(p1.SafeEntryList.Count != 0)
                                {
                                    if (r1.Token != null)
                                    {
                                        Console.WriteLine("{0, -7} {1, -15} {2, -25} {3, -13} {4, -15} {5, -22} {6, -22} {7, -20} {8}", 
                                            "Name", "Address", "Last Left Country", "Check In", "Check Out", "Location", "Serial No", "Collection Location", "Expiry Date");
                                        Console.WriteLine("----------------------------------------------------------------------------------------------------------------------------------------------------------------------");
                                        Console.WriteLine("{0, -7} {1, -15} {2, -25} {3, -13} {4, -15} {5, -22} {6, -22} {7, 20} {8}",
                                            r1.Name, r1.Address, r1.LastLeftCountry, p1.SafeEntryList[0].CheckIn, p1.SafeEntryList[0].CheckOut, p1.SafeEntryList[0].Location.BusinessName, r1.Token.SerialNo, r1.Token.CollectionLocation, r1.Token.ExpiryDate);
                                        Console.WriteLine();
                                        break;
                                    }
                                    else
                                    {
                                        Console.WriteLine("{0, -7} {1, -15} {2, -25} {3, -13} {4, -15} {5}",
                                            "Name", "Address", "Last Left Country", "Check In", "Check Out", "Location");
                                        Console.WriteLine("----------------------------------------------------------------------------------------------------------------------------------------------------------------------");
                                        Console.WriteLine("{0, -7} {1, -15} {2, -25} {3, -13} {4, -15} {5}",
                                            r1.Name, r1.Address, r1.LastLeftCountry, p1.SafeEntryList[0].CheckIn, p1.SafeEntryList[0].CheckOut, p1.SafeEntryList[0].Location.BusinessName);
                                        Console.WriteLine();
                                        break;
                                    }
                                }
                                else if(r1.Token != null)
                                {
                                    Console.WriteLine("{0, -7} {1, -15} {2, -25} {3, -13} {4, -15} {5}",
                                        "Name", "Address", "Last Left Country", "Serial No", "Collection Location", "Expiry Date");
                                    Console.WriteLine("----------------------------------------------------------------------------------------------------------------------------------------------------------------------");
                                    Console.WriteLine("{0, -7} {1, -15} {2, -25} {3, -13} {4, -15} {5}",
                                        r1.Name, r1.Address, r1.LastLeftCountry, r1.Token.SerialNo, r1.Token.CollectionLocation, r1.Token.ExpiryDate);
                                    Console.WriteLine();
                                    break;
                                }
                                else
                                {
                                    Console.WriteLine("{0, -7} {1, -15} {2, -25}",
                                        "Name", "Address", "Last Left Country");
                                    Console.WriteLine("-----------------------------------------------------------------------");                        
                                    Console.WriteLine("{0, -7} {1, -15} {2, -25}",
                                        r1.Name, r1.Address, r1.LastLeftCountry);
                                    Console.WriteLine();
                                    break;
                                }
                            }
                            else
                            {
                                Visitor v1 = SearchVisitor(vList, p1.Name);
                                if (v1.TravelEntryList.Count != 0)
                                {
                                    if (p1.SafeEntryList.Count != 0)
                                    {
                                        if (p1.SafeEntryList[0].CheckOut != DateTime.MinValue)
                                        {
                                            Console.WriteLine("{0, -6} {1, -13} {2, -13} {3, -13} {4, -13} {5, -25} {6, -25} {7,-22} {8}",
                                                "Name", "Passport No", "Nationality", "Last Country", "Entry Mode", "Entry Date", "Check In", "Check Out", "Location");
                                            Console.WriteLine("-------------------------------------------------------------------------------------------------------------------------------------------------------");
                                            Console.WriteLine("{0, -6} {1, -13} {2, -13} {3, -13} {4, -13} {5, -25} {6, -25} {7,-22} {8}",
                                                v1.Name, v1.PassportNo, v1.Nationality, v1.TravelEntryList[0].LastCountryOfEmbarkation, v1.TravelEntryList[0].EntryMode, v1.TravelEntryList[0].EntryDate, p1.SafeEntryList[0].CheckIn, p1.SafeEntryList[0].CheckOut, p1.SafeEntryList[0].Location.BusinessName);
                                            Console.WriteLine();
                                            break;
                                        }
                                        else
                                        {
                                            Console.WriteLine("{0, -6} {1, -13} {2, -13} {3, -13} {4, -13} {5, -25} {6, -25} {7,-15} {8}",
                                                "Name", "Passport No", "Nationality", "Last Country", "Entry Mode", "Entry Date", "Check In", "Check Out", "Location");
                                            Console.WriteLine("----------------------------------------------------------------------------------------------------------------------------------------------------------");
                                            Console.WriteLine("{0, -6} {1, -13} {2, -13} {3, -13} {4, -13} {5, -25} {6, -25} {7,-15} {8}",
                                                v1.Name, v1.PassportNo, v1.Nationality, v1.TravelEntryList[0].LastCountryOfEmbarkation, v1.TravelEntryList[0].EntryMode, v1.TravelEntryList[0].EntryDate, p1.SafeEntryList[0].CheckIn, "-", p1.SafeEntryList[0].Location.BusinessName);
                                            Console.WriteLine();
                                            break;
                                        }
                                    }
                                    else
                                    {
                                        Console.WriteLine("{0, -6} {1, -13} {2, -13} {3,-13} {4,-13} {5}",
                                                "Name", "Passport No", "Nationality", "Last Country", "Entry Mode", "Entry Date");
                                        Console.WriteLine("------------------------------------------------------------------------------------------------------------------------");
                                        Console.WriteLine("{0,-6} {1,-13} {2,-13} {3,-13} {4,-13} {5}",
                                            v1.Name, v1.PassportNo, v1.Nationality, v1.TravelEntryList[0].LastCountryOfEmbarkation, v1.TravelEntryList[0].EntryMode, v1.TravelEntryList[0].EntryDate);
                                        Console.WriteLine();
                                        break;
                                    }
                                }
                                else if (p1.SafeEntryList.Count != 0)
                                {
                                    if (p1.SafeEntryList[0].CheckOut != DateTime.MinValue)
                                    {
                                        Console.WriteLine("{0,-6} {1,-13} {2,-13} {3,-25} {4,-22} {5}",
                                            "Name", "Passport No", "Nationality", "Check In", "Check Out", "Location");
                                        Console.WriteLine("--------------------------------------------------------------------------------------------------------------------------");
                                        Console.WriteLine("{0,-6} {1,-13} {2,-13} {3,-25} {4,-15} {5}",
                                            v1.Name, v1.PassportNo, v1.Nationality, p1.SafeEntryList[0].CheckIn, p1.SafeEntryList[0].CheckOut, p1.SafeEntryList[0].Location.BusinessName);
                                        Console.WriteLine();
                                        break;
                                    }
                                    else
                                    {
                                        Console.WriteLine("{0,-6} {1,-13} {2,-13} {3,-25} {4,-15} {5}",
                                                "Name", "Passport No", "Nationality", "Check In", "Check Out", "Location");
                                        Console.WriteLine("-------------------------------------------------------------------------------------------------------------------");
                                        Console.WriteLine("{0,-6} {1,-13} {2,-13} {3,-25} {4,-15} {5}",
                                            v1.Name, v1.PassportNo, v1.Nationality, p1.SafeEntryList[0].CheckIn, "-", p1.SafeEntryList[0].Location.BusinessName);
                                        Console.WriteLine();
                                        break;
                                    }
                                }
                                else
                                {
                                    Console.WriteLine("{0,-6} {1,-13} {2}",
                                        "Name", "Passport No", "Nationality");
                                    Console.WriteLine("---------------------------------------------------");
                                    Console.WriteLine("{0,-6} {1,-13} {2}",
                                        v1.Name, v1.PassportNo, v1.Nationality);
                                    Console.WriteLine();
                                    break;
                                }
                            }
                        }
                        else
                        {
                            Console.WriteLine("Invalid Name.");
                            Console.WriteLine("Please Try Again.");
                            Console.WriteLine();
                            continue;
                        }
                    }
                }
                else if (option == "3")  //assign and replace tracetogether token
                {
                    Console.Write("Enter Your Name (Or 0 to Exit): ");
                    string name = Console.ReadLine();
                    if(name == "0") // To enable users to exit to main menu whenever they want
                    {
                        Console.WriteLine();
                        continue;
                    }
                    Resident r = SearchResident(rList, name);
                    if (r == null)
                    {
                        Console.WriteLine("Person does not exist or He is not a Resident\n");
                        Console.WriteLine();
                        continue;
                    }
                    else
                    {
                        if(r.Token== null)
                        {
                            Console.WriteLine("You Currently do not have a TraceTogether Token.");
                            Console.Write("Enter New Serial Number: ");
                            string sNum = Console.ReadLine();
                            r.Token= new TraceTogetherToken(sNum,AssignCollectionPoint(collectionList), DateTime.Now.AddMonths(6));
                            Console.WriteLine("Your New Trace Together Token ({0}) has been created", r.Token.SerialNo);
                            Console.WriteLine();
                            continue;
                        }
                        else
                        {
                            if (r.Token.IsEligibleForReplacement() == true)
                            {
                                Console.WriteLine("Your Trace Together Token is eligible for replacement");
                                Console.Write("Enter New Serial Number: ");
                                string sNum = Console.ReadLine();
                                Console.Write("Enter Collection Location: ");
                                string cLoc = Console.ReadLine();
                                r.Token.ReplaceToken(sNum, cLoc);
                                Console.WriteLine("Your Replacement Token has just been created.\n");
                                continue;
                            }/*
                                else
                                {
                                    ttt.ExpiryDate.AddMonths(6);
                                    ttt.SerialNo = "T"+SerialNumberGenerator(tttList);
                                    ttt.CollectionLocation = AssignCollectionPoint(collectionList);
                                    Resident r1 = SearchResident(rList, name);
                                    r1.Token.ExpiryDate.AddMonths(6);
                                    Console.WriteLine("Your Trace Together Token is eligible for replacement\n" +
                                        "Collect your new Trace Together Token at {0}",ttt.CollectionLocation);
                                    Console.WriteLine();
                                    break;
                                }*/
                            else
                            {
                                Console.WriteLine("Your Token does not meet the requirement to replace\n");
                                continue;
                            }

                        }
                    }
                }
                else if (option == "4") // Display Business Location
                {
                    DisplayBusinessLocation(blList);
                    Console.WriteLine();
                }
                else if (option == "5") // Edit Business Location Details
                {
                    DisplayBusinessLocation(blList);
                    Console.WriteLine();
                    while (true)
                    {
                        Console.Write("Enter Business Name (Or 0 to Exit): ");
                        string bname = Console.ReadLine();
                        if(bname == "0") // To enable users to exit to main menu whenever they want
                        {
                            Console.WriteLine();
                            break;
                        }
                        if (SearchBusinessLocation(blList, bname) != null)
                        {
                            Console.Write("Enter New Max Capacity (Or 0 to Exit): ");
                            try
                            {
                                int maxcap = Convert.ToInt32(Console.ReadLine());
                                if(maxcap == 0)
                                {
                                    Console.WriteLine();
                                    break;
                                }
                                SearchBusinessLocation(blList, bname).MaximumCapacity = maxcap;
                                Console.WriteLine("Max Capacity for {0} has been changed to {1} successfully!", bname, maxcap);
                                Console.WriteLine();
                                DisplayBusinessLocation(blList);
                                Console.WriteLine();
                                break;
                            }
                            catch
                            {
                                Console.WriteLine("Please Enter a Valid Number!");
                                continue;
                            }
                        }
                        else
                        {
                            Console.WriteLine("Business Location Currently does not Exist.");
                            continue;
                        }
                    }
                }
                else if (option == "6") // SafeEntry Check In
                {
                    while (true)
                    {
                        Console.Write("Enter name (Or 0 to Exit): ");
                        string name = Console.ReadLine();
                        if(name == "0") // To enable users to exit to main menu whenever they want
                        {
                            Console.WriteLine();
                            break;
                        }
                        Person p1 = SearchForPerson(pList, name);
                        if (p1 != null)
                        {
                            Console.WriteLine();
                            DisplayBusinessLocation(blList);
                            Console.WriteLine();
                            Console.Write("Enter Business Name (Or 0 to Exit): ");
                            string bname = Console.ReadLine();
                            if(bname == "0") // To enable users to exit to main menu whenever they want
                            {
                                Console.WriteLine();
                                break;
                            }
                            BusinessLocation bl1 = SearchBusinessLocation(blList, bname);
                            if (bl1 != null)
                            {
                                if (bl1.VisitorsNow != bl1.MaximumCapacity)
                                {
                                    SafeEntry se = new SafeEntry(DateTime.Now, bl1);
                                    p1.AddSafeEntry(se);
                                    if(SearchResident(rList, p1.Name) != null)
                                    {
                                        SearchResident(rList, p1.Name).AddSafeEntry(se);
                                    }
                                    else if (SearchVisitor(vList, p1.Name) != null)
                                    {
                                        SearchVisitor(vList, p1.Name).AddSafeEntry(se);
                                    }
                                    bl1.VisitorsNow += 1;
                                    Console.WriteLine("SafeEntry Checked In Successfully!");
                                    seList.Add(se);
                                    Console.WriteLine();
                                    break;
                                }
                                else
                                {
                                    Console.WriteLine("{0} has currently reached Max Capacity.", bl1.BusinessName);
                                    Console.WriteLine("Please either visit another location or come back at a later time.");
                                    Console.WriteLine();
                                    continue;
                                }
                            }
                            else
                            {
                                Console.WriteLine("Please Enter a Valid Name.\n");
                                continue;
                            }
                        }
                        else
                        {
                            Console.WriteLine("Please Enter a Valid Name.");
                            Console.WriteLine();
                            continue;
                        }
                    }
                }
                else if (option == "7") // SafeEntry Check Out
                {
                    while (true)
                    {
                        Console.Write("Enter Name (Or 0 to Exit): ");
                        string name = Console.ReadLine();
                        if (name == "0") // To enable users to exit to main menu whenever they want
                        {
                            Console.WriteLine();
                            break;
                        }
                        Person p1 = SearchForPerson(pList, name);
                        if(p1 != null)
                        {
                            Console.WriteLine();
                            DisplaySafeEntry(p1);
                            Console.WriteLine();
                            Console.Write("Enter Number (Or 0 to Exit): ");
                            try
                            {
                                int num = Convert.ToInt32(Console.ReadLine());
                                if(num == 0) // To enable users to exit to main menu whenever they want
                                {
                                    Console.WriteLine();
                                    break;
                                }
                                Console.WriteLine();
                                SafeEntry se = SearchSafeEntry(p1, num);
                                if (se != null)
                                {
                                    se.PerformCheckOut();
                                    se.Location.VisitorsNow -= 1;
                                    Console.WriteLine("Safe Entry Check Out was Successful.");
                                    Console.WriteLine();
                                    break;
                                }
                                else
                                {
                                    Console.WriteLine("Safe Entry Record has either already been checked out or Invalid Choice");
                                    Console.WriteLine();
                                    break;
                                }
                            }
                            catch
                            {
                                Console.WriteLine("Please Enter a Valid Number.");
                                Console.WriteLine();
                                continue;
                            }
                        }
                        else
                        {
                            Console.WriteLine("Please Enter a Valid Name.");
                            Console.WriteLine();
                            continue;
                        }
                    }
                }
                else if (option == "8") // Display SHN Facility
                {
                    DisplaySHNFacility(shnList);
                    Console.WriteLine();
                }
                else if (option == "9") // Create New Visitor
                {
                    while (true)
                    {
                        Console.Write("Enter Name (Or 0 to Exit): ");
                        string name = Console.ReadLine();
                        if (name == "0") // To enable users to exit to main menu whenever they want
                        {
                            Console.WriteLine();
                            break;
                        }
                        Console.Write("Enter Passport No (Or 0 to Exit): ");
                        string passportNo = Console.ReadLine();
                        if (passportNo == "0") // To enable users to exit to main menu whenever they want
                        {
                            Console.WriteLine();
                            break;
                        }
                        Console.Write("Nationality (Or 0 to Exit): ");
                        string nationality = Console.ReadLine();
                        if (nationality == "0") // To enable users to exit to main menu whenever they want
                        {
                            Console.WriteLine();
                            break;
                        }
                        else if (nationality == "Singaporean")
                        {
                            Console.Write("Visitor cannot be Singaporean\n\n");
                            break;
                            
                        }
                        Visitor vCheck = SearchVisitor(vList, name);
                        if (vCheck == null)
                        {
                            Visitor v1 = new Visitor(name, passportNo, nationality);
                            vList.Add(v1);
                            pList.Add(v1);
                            Console.WriteLine("New Visitor, {0}, has been successfuly added!", name);
                            Console.WriteLine();
                            break;
                        }
                        else
                        {
                            Console.WriteLine("Visitor Already Exists!");
                            Console.WriteLine("Please Try Again");
                            continue;
                        }
                    }
                }
                else if (option == "10") // Create new TravelEntry Record
                {
                    while (true)
                    {
                        Console.Write("Enter Name (Or 0 to Exit): ");
                        string name = Console.ReadLine();
                        if (name == "0") // To enable the user to exit to main menu whenever they want
                        {
                            Console.WriteLine();
                            break;
                        }
                        Person p1 = SearchForPerson(pList, name);
                        if(p1 != null)
                        {
                            Console.Write("Enter Last Country of Embarkation (Or 0 to Exit): ");
                            string lastCountryOfEmbarkation = Console.ReadLine();
                            if(lastCountryOfEmbarkation == "0") // To enable the user to exit to main menu whenever they want
                            {
                                Console.WriteLine();
                                break;
                            }

                            Console.Write("Enter Entry Mode (Or 0 to Exit): ");
                            string entryMode = Console.ReadLine();
                            if(entryMode == "0") // To enable the user to exit to main menu whenever they want
                            {
                                Console.WriteLine();
                                break;
                            }
                            Console.WriteLine();
                            DateTime entryDate = DateTime.Now;
                            TravelEntry te1 = new TravelEntry(lastCountryOfEmbarkation, entryMode, entryDate);
                            te1.CalculateSHNDuration();
                            if (te1.ShnEndDate == entryDate.AddDays(14))
                            {
                                DisplaySHNFacility(shnList);
                                Console.WriteLine();
                                Console.Write("Select a SHN Facility (Or 0 to Exit): ");
                                string facilityName = Console.ReadLine();
                                if (facilityName == "0") // To enable user to exit to main menu whenver they want
                                {
                                    Console.WriteLine();
                                    break;
                                }
                                SHNFacility shn1 = SearchSHNFacility(shnList, facilityName);
                                if(shn1 != null)
                                {
                                    if(shn1.FacilityVacancy != 0)
                                    {
                                        te1.AssignSHNFacility(shn1);
                                        shn1.FacilityVacancy -= 1;
                                        p1.AddTravelEntry(te1);
                                        if(SearchResident(rList, name) != null)
                                        {
                                            SearchResident(rList, name).AddTravelEntry(te1);
                                        }
                                        else if(SearchVisitor(vList, name) != null)
                                        {
                                            SearchVisitor(vList, name).AddTravelEntry(te1);
                                        }
                                        Console.WriteLine("Successfully Assigned SHN Facility to {0}", name);
                                        Console.WriteLine();
                                        Console.WriteLine("You have been assigned to 14 Days of SHN at {0} and a Swab Test.\n", te1.ShnStay.FacilityName);
                                        break;
                                    }
                                    else
                                    {
                                        Console.WriteLine("{0} currently has no vacancy.", facilityName);
                                        Console.WriteLine("Please Choose another SHN Facility");
                                        Console.WriteLine();
                                        continue;
                                    }
                                }
                                else
                                {
                                    Console.WriteLine("Invalid Choice.");
                                    Console.WriteLine();
                                    continue;
                                }
                            }
                            else if(te1.ShnEndDate == entryDate.AddDays(7))
                            {
                                Console.WriteLine("You have been assigned to 7 Days of SHN at your own Accommodation and a Swab Test.\n");
                                p1.AddTravelEntry(te1);
                                if (SearchResident(rList, name) != null)
                                {
                                    SearchResident(rList, name).AddTravelEntry(te1);
                                }
                                else if (SearchVisitor(vList, name) != null)
                                {
                                    SearchVisitor(vList, name).AddTravelEntry(te1);
                                        }
                                break;
                            }
                            else if(te1.ShnEndDate == entryDate)
                            {
                                Console.WriteLine("You have been assigned to 0 days of SHN and a Swab Test.\n");
                                p1.AddTravelEntry(te1);
                                if (SearchResident(rList, name) != null)
                                {
                                    SearchResident(rList, name).AddTravelEntry(te1);
                                }
                                else if (SearchVisitor(vList, name) != null)
                                {
                                    SearchVisitor(vList, name).AddTravelEntry(te1);
                                        }
                                break;
                            }
                        }
                        else
                        {
                            Console.WriteLine("Please Enter a Valid Name.");
                            Console.WriteLine();
                            continue;
                        }
                    }
                }
                else if (option == "11") // Calculate SHN Charges
                {
                    while (true)
                    {
                        bool check = false;
                        Console.Write("Enter Name (Or 0 to Exit): ");
                        string name = Console.ReadLine();
                        if(name == "0") //Enables user to exit to main menu whenever
                        {
                            Console.WriteLine();
                            break;
                        }
                        Person p1 = SearchForPerson(pList, name);
                        if(p1 != null)
                        {
                            foreach(TravelEntry te in p1.TravelEntryList)
                            {
                                if (te.ShnEndDate < DateTime.Now && te.IsPaid == false)
                                {
                                    double charges = p1.CalculateSHNCharges(); //NOT COMPLETE...INCOMPLETE...TO BE COMPLETED
                                    if (charges == 2200)
                                    {
                                        if (te.ShnStay != null)
                                        {
                                            double travelCost = te.ShnStay.CalculateTravelCost(te.EntryMode, te.EntryDate);
                                            if (travelCost != 0)
                                            {
                                                charges += travelCost;
                                                double gst = (charges / 100) * 7;
                                                charges += gst;
                                                Console.WriteLine("You Currently Owe {0:0.00}.", charges);
                                                Console.Write("Pay? (Y/N): ");
                                                string choice = Console.ReadLine();
                                                if (choice == "Y")
                                                {
                                                    te.IsPaid = true;
                                                    te.ShnStay.FacilityVacancy += 1;
                                                    Console.WriteLine("Payment was Successful.\n");
                                                    check = true;
                                                    break;
                                                }
                                                else if (choice == "N")
                                                {
                                                    Console.WriteLine("Payment was cancelled\n");
                                                    check = true;
                                                    break;
                                                }
                                                else
                                                {
                                                    Console.WriteLine("Invalid Option.\nPlease Try Again.\n");
                                                }
                                            }
                                            else
                                            {
                                                Console.WriteLine("Error.\nTry Again.");
                                                continue;
                                            }
                                        }
                                        else
                                        {
                                            Console.WriteLine("You haven't been assigned to a SHN Facility.\n");
                                            DisplaySHNFacility(shnList);
                                            Console.Write("Enter Facility Name (Or 0 to Exit): ");
                                            string facilityName = Console.ReadLine();
                                            if (facilityName == "0")
                                            {
                                                Console.WriteLine();
                                                break;
                                            }
                                            SHNFacility shn1 = SearchSHNFacility(shnList, facilityName);
                                            if (shn1 != null)
                                            {
                                                if (shn1.FacilityVacancy != 0)
                                                {
                                                    te.AssignSHNFacility(shn1);
                                                    shn1.FacilityVacancy -= 1;
                                                    p1.AddTravelEntry(te);
                                                    Console.WriteLine("Successfully Assigned SHN Facility to {0}", name);
                                                    Console.WriteLine();
                                                    Console.WriteLine("You have been assigned to 14 Days of SHN at {0} and a Swab Test.\n", te.ShnStay.FacilityName);
                                                    check = true;
                                                    break;
                                                }
                                                else
                                                {
                                                    Console.WriteLine("{0} currently has no vacancy.", facilityName);
                                                    Console.WriteLine("Please Choose another SHN Facility");
                                                    Console.WriteLine();
                                                    check = true;
                                                    continue;
                                                }
                                            }
                                            else
                                            {
                                                Console.WriteLine("Invalid Choice.");
                                                Console.WriteLine();
                                                check = true;
                                                continue;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        double gst = (charges / 100) * 7;
                                        charges += gst;
                                        Console.WriteLine("You Currently Owe {0:0.00}.", charges);
                                        Console.Write("Pay? (Y/N): ");
                                        string choice = Console.ReadLine();
                                        if (choice == "Y")
                                        {
                                            te.IsPaid = true;
                                            Console.WriteLine("Payment was Successful.\n");
                                            check = true;
                                            break;
                                        }
                                        else if (choice == "N")
                                        {
                                            Console.Write("Payment was Cancelled\n");
                                            check = true;
                                            break;
                                        }
                                        else
                                        {
                                            Console.WriteLine("Invalid Choice.\nPlease Try Again.\n");
                                            continue;
                                        }
                                    }
                                }
                            }
                            if(check == false)
                            {
                                Console.WriteLine("Error.\n" +
                                    "Either SHN Stay has not ended or has already been Paid.\n");
                            }
                            
                        }
                        else
                        {
                            Console.WriteLine("{0} does not currently exist.", name);
                            Console.WriteLine("Please Try again.\n");
                            continue;

                        }
                    }
                }
                else if (option == "12")
                {
                    StreamWriter data = new StreamWriter(@"ContactTracingReport.csv", true);
                    while (true)
                    {
                        Console.Write("Enter Date (Day/Month/Year): ");
                        try
                        {
                            DateTime givenDate = Convert.ToDateTime(Console.ReadLine());
                            Console.Write("Enter Business Name (Or 0 to Exit): ");
                            string bname = Console.ReadLine();
                            if (bname == "0")
                            {
                                Console.WriteLine();
                                break;
                            }
                            BusinessLocation b1 = SearchBusinessLocation(blList, bname);
                            if(b1 != null)
                            {
                                
                                foreach(Person p in pList)
                                {
                                    foreach (SafeEntry se in p.SafeEntryList)
                                    {
                                        if (se.Location.BusinessName == bname)
                                        {
                                            if (se.CheckIn.Date == givenDate.Date)
                                            {
                                                if(se.CheckOut != DateTime.MinValue)
                                                {;
                                                    string data1 = p.Name + "," + Convert.ToString(se.CheckIn) + "," + Convert.ToString(se.CheckOut) + "," + se.Location.BusinessName;
                                                    data.WriteLine(data1);
                                                }
                                                else
                                                {
                                                    string data1 = p.Name + "," + Convert.ToString(se.CheckIn) + "," + "-" + "," + se.Location.BusinessName;
                                                    data.WriteLine(data1);
                                                }
                                            }
                                            else
                                            {
                                                continue;
                                            }
                                        }
                                        else
                                        {
                                            continue;
                                        }
                                    }
                                }
                                data.Close();
                                Console.WriteLine("Please Check ContactTracingReport.CSV for results.\n");
                                break;
                            }
                            else
                            {
                                Console.WriteLine("Invalid Input.\nPlease Try Again.\n");
                                continue;
                            }
                        }
                        catch
                        {
                            Console.WriteLine("Error.\nInvalid Input.\nPlease Try Again.");
                            continue;
                        }

                    }
                }
                else if (option == "13")
                {
                    StreamWriter data = new StreamWriter(@"SHNStatusReport.csv", true);
                    while(true)
                    {
                        Console.Write("Enter Date (Day/Month/Year): ");
                        try
                        {
                            DateTime givenDate = Convert.ToDateTime(Console.ReadLine());
                            data.WriteLine("Name, SHN End Date, SHN Facility");
                            foreach (Person p in pList)
                            {
                                foreach (TravelEntry te in p.TravelEntryList)
                                {
                                    if(te.EntryDate.Date <= givenDate.Date && te.ShnEndDate.Date >= givenDate.Date )
                                    {
                                        if(te.ShnStay != null)
                                        {
                                            string data1 = p.Name + "," + Convert.ToString(te.ShnEndDate) + "," + te.ShnStay.FacilityName;
                                            data.WriteLine(data1);
                                        }
                                        else if((te.ShnEndDate.Day - te.EntryDate.Day) == 7)
                                        {
                                            string data1 = p.Name + "," + Convert.ToString(te.ShnEndDate) + "," + "Home";
                                            data.WriteLine(data1);
                                        }
                                    }
                                }
                            }
                            data.Close();
                            Console.WriteLine("Please Check SHNStatusReport.CSV for results.\n");
                            break;
                        }
                        catch
                        {
                            Console.WriteLine("Error.\nInvalid Input.\nPlease Try Again.");
                            continue;
                        }
                    }
                }
                else if (option == "0")
                {
                    Console.WriteLine("Thanks for using our Program!");
                    Console.WriteLine("Program Successfuly Exited");
                    break;
                }
                else
                {
                    Console.WriteLine("Invalid Input!");
                    Console.WriteLine("Please enter a number!");
                    Console.WriteLine();
                }
            }
        }
        static void DisplayMenu()
        {
            Console.WriteLine("Menu");
            Console.WriteLine("-----------------------------");
            Console.WriteLine("[1] Display Vistors");
            Console.WriteLine("[2] Display Person");
            Console.WriteLine("[3] Assign/Replace Tracetogether Token");
            Console.WriteLine("[4] Display Business Locations");
            Console.WriteLine("[5] Edit Business Location Capacity");
            Console.WriteLine("[6] SafeEntry Check In");
            Console.WriteLine("[7] SafeEntry Check Out");
            Console.WriteLine("[8] Diplay SHN Facility");
            Console.WriteLine("[9] New Visitor");
            Console.WriteLine("[10] New TravelEntry Record");
            Console.WriteLine("[11] Calculate SHN Charges");
            Console.WriteLine("[12] Contact Tracing Report");
            Console.WriteLine("[13] SHN Status Report");
            Console.WriteLine("[0] Exit Program");
        }

        static void LoadPersonData(List<Person> pList, List<Resident> rList, List<Visitor> vList, List<TraceTogetherToken> tttList, List<SHNFacility> shnList)
        {
            string[] csvLines = File.ReadAllLines("Person.csv");
            for (int i = 1; i < csvLines.Length; i++)
            {
                string[] data = csvLines[i].Split(",");
                string name = data[1];
                //bool isPaid = Convert.ToBoolean(data[13]);
                if (data[0] == "resident")
                {
                    string address = data[2];
                    DateTime lastLeftCountry = DateTime.ParseExact(data[3], "d/M/yyyy", CultureInfo.CurrentCulture);
                    if (data[9] == "")
                    {
                        if (data[8] == "")
                        {
                            Person p1 = new Resident(name, address, lastLeftCountry);
                            Resident r1 = new Resident(name, address, lastLeftCountry);
                            pList.Add(p1);
                            rList.Add(r1);
                        }
                        else
                        {
                            string serialNo = data[6];
                            string collectionLoc = data[7];
                            DateTime expiryDate = DateTime.ParseExact(data[8], "d/M/yyyy", CultureInfo.CurrentCulture);
                            Person p1 = new Resident(name, address, lastLeftCountry);
                            Resident r1 = new Resident(name, address, lastLeftCountry);
                            TraceTogetherToken tt1 = new TraceTogetherToken(serialNo, collectionLoc, expiryDate);
                            r1.Token = tt1;
                            pList.Add(p1);
                            rList.Add(r1);
                            tttList.Add(tt1);
                        }
                    }
                    else
                    {
                        string travelEntryLastCountry = data[9];
                        string entryMode = data[10];
                        DateTime entryDate = DateTime.ParseExact(data[11], "d/M/yyyy h:mm", CultureInfo.CurrentCulture);
                        DateTime shnEndDate = DateTime.ParseExact(data[12], "d/M/yyyy h:mm", CultureInfo.CurrentCulture);
                        bool isPaid = Convert.ToBoolean(data[13]);
                        string fname = data[14];
                        if (data[8] == "")
                        {
                            SHNFacility shn1 = SearchSHNFacility(shnList, fname);
                            if (shn1 != null)
                            {
                                Person p1 = new Resident(name, address, lastLeftCountry);
                                TravelEntry t1 = new TravelEntry(travelEntryLastCountry, entryMode, entryDate);
                                t1.AssignSHNFacility(shn1);
                                if (shnEndDate > DateTime.Now)
                                {
                                    shn1.FacilityVacancy -= 1;
                                }
                                t1.ShnEndDate = shnEndDate;
                                t1.IsPaid = isPaid;
                                p1.AddTravelEntry(t1);
                                Resident r1 = new Resident(name, address, lastLeftCountry);
                                r1.AddTravelEntry(t1);
                                pList.Add(p1);
                                rList.Add(r1);
                            }
                            else
                            {
                                Person p1 = new Resident(name, address, lastLeftCountry);
                                TravelEntry t1 = new TravelEntry(travelEntryLastCountry, entryMode, entryDate);
                                t1.ShnEndDate = shnEndDate;
                                t1.IsPaid = isPaid;
                                p1.AddTravelEntry(t1);
                                Resident r1 = new Resident(name, address, lastLeftCountry);
                                r1.AddTravelEntry(t1);
                                pList.Add(p1);
                                rList.Add(r1);
                            }
                        }
                        else
                        {
                            string serialNo = data[6];
                            string collectionLoc = data[7];
                            DateTime expiryDate = DateTime.ParseExact(data[8], "d/M/yyyy", CultureInfo.CurrentCulture);
                            TravelEntry t1 = new TravelEntry(travelEntryLastCountry, entryMode, expiryDate);
                            Person p1 = new Resident(name, address, lastLeftCountry);
                            TraceTogetherToken tt1 = new TraceTogetherToken(serialNo, collectionLoc, expiryDate);
                            Resident r1 = new Resident(name, address, lastLeftCountry);
                            t1.IsPaid = isPaid;
                            t1.ShnEndDate = shnEndDate;
                            SHNFacility shn1 = SearchSHNFacility(shnList, fname);
                            if (shn1 != null)
                            {
                                t1.AssignSHNFacility(shn1);
                                if(shnEndDate > DateTime.Now)
                                {
                                    shn1.FacilityVacancy -= 1;
                                }
                                p1.AddTravelEntry(t1);
                                r1.Token = tt1;
                                r1.AddTravelEntry(t1);
                                pList.Add(p1);
                                rList.Add(r1);
                                tttList.Add(tt1);
                            }
                            else
                            {
                                p1.AddTravelEntry(t1);
                                r1.Token = tt1;
                                r1.AddTravelEntry(t1);
                                pList.Add(p1);
                                rList.Add(r1);
                                tttList.Add(tt1);
                            }
                        }
                    }
                }
                else if (data[0] == "visitor")
                {
                    string passportNo = data[4];
                    string nationality = data[5];
                    if (data[9] == "")
                    {
                        Person p2 = new Visitor(name, passportNo, nationality);
                        Visitor v1 = new Visitor(name, passportNo, nationality);
                        pList.Add(p2);
                        vList.Add(v1);
                    }
                    else
                    {
                        string travelEntryLastCountry = data[9];
                        string entryMode = data[10];
                        DateTime entryDate = DateTime.ParseExact(data[11], "d/M/yyyy h:mm", CultureInfo.CurrentCulture);
                        DateTime shnEndDate = DateTime.ParseExact(data[12], "d/M/yyyy h:mm", CultureInfo.CurrentCulture);
                        bool isPaid = Convert.ToBoolean(data[13]);
                        string fname = data[14];
                        TravelEntry t1 = new TravelEntry(travelEntryLastCountry, entryMode, entryDate);
                        Person p2 = new Visitor(name, passportNo, nationality);
                        Visitor v1 = new Visitor(name, passportNo, nationality);
                        t1.IsPaid = isPaid;
                        t1.ShnEndDate = shnEndDate;
                        SHNFacility shn1 = SearchSHNFacility(shnList, fname);
                        if (shn1 != null)
                        {
                            t1.AssignSHNFacility(shn1);
                            if (shnEndDate > DateTime.Now)
                            {
                                shn1.FacilityVacancy -= 1;
                            }
                            p2.AddTravelEntry(t1);
                            v1.AddTravelEntry(t1);
                            pList.Add(p2);
                            vList.Add(v1);
                        }
                        else
                        {
                            p2.AddTravelEntry(t1);
                            v1.AddTravelEntry(t1);
                            pList.Add(p2);
                            vList.Add(v1);
                        }
                    }
                }
            }
        }
        static void LoadBusinessLocationData(List<BusinessLocation> blList)
        {
            string[] csvLines = File.ReadAllLines("BusinessLocation.csv");
            for (int i = 1; i < csvLines.Length; i++)
            {
                string[] data = csvLines[i].Split(",");
                string bname = data[0];
                string bcode = data[1];
                int capacity = Convert.ToInt32(data[2]);
                BusinessLocation b1 = new BusinessLocation(bname, bcode, capacity);
                blList.Add(b1);
            }
        }
        static void LoadSHNData(List<SHNFacility> shnList)
        {
            List<SHNFacility> shnList1 = new List<SHNFacility>();
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://covidmonitoringapiprg2.azurewebsites.net");
                Task<HttpResponseMessage> responseTask = client.GetAsync("/facility");
                responseTask.Wait();
                HttpResponseMessage result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    Task<String> readTask = result.Content.ReadAsStringAsync();
                    readTask.Wait();
                    string data = readTask.Result;
                    shnList1 = JsonConvert.DeserializeObject<List<SHNFacility>>(data);
                    foreach(SHNFacility shn in shnList1)
                    {
                        shn.FacilityVacancy = shn.FacilityCapacity;
                        shnList.Add(shn);
                    }
                }
            }
        }
        static void DisplayVisitor(List<Visitor> vList)
        {
            Console.WriteLine("Visitor List:");
            Console.WriteLine("---------------------------------------------------------------------------------------------------");
            Console.WriteLine("{0, -8} | {1, -12} | {2, -12} | {3, -29} | {4, -12} | {5}", "Name", "Passport No", "Nationality", "Last Country of Embarkation", "Entry Mode", "Entry Date");
            Console.WriteLine("---------------------------------------------------------------------------------------------------");
            foreach (Visitor v in vList)
            {
                if(v.TravelEntryList.Count == 0 )
                {
                    Console.WriteLine("{0, -10} {1, -14} {2}",
                        v.Name, v.PassportNo, v.Nationality);                
                }
                else
                {
                    Console.WriteLine("{0, -10} {1, -14} {2, -14} {3, -31} {4, -14} {5}",
                        v.Name, v.PassportNo, v.Nationality, v.TravelEntryList[0].LastCountryOfEmbarkation, v.TravelEntryList[0].EntryMode, v.TravelEntryList[0].EntryDate);
                }
            }
        }
        static void DisplayPerson(List<Person> pList)
        {
            Console.WriteLine("Person List:");
            Console.WriteLine("---------------------");
            Console.WriteLine("Name");
            foreach(Person p in pList)
            {
                Console.WriteLine(p.Name);
            }
        }
        static void DisplayBusinessLocation(List<BusinessLocation> blList)
        {
            Console.WriteLine("{0, -25} {1, -20} {2, -20} {3}",
                "Business Name", "Branch Code", "Maximum Capacity", "Visitors Now");
            Console.WriteLine("----------------------------------------------------------------------------------------------");
            foreach (BusinessLocation bLoc in blList)
            {
                Console.WriteLine("{0, -25} {1, -20} {2, -20} {3}",
                    bLoc.BusinessName, bLoc.BranchCode, bLoc.MaximumCapacity, bLoc.VisitorsNow);
            }
        }
        static void DisplaySHNFacility(List<SHNFacility> shnList)
        {
            Console.WriteLine("{0, -18} {1, -18} {2, -18} {3, -18} {4, -18} {5}",
                "Facility Name", "Facility Capacity", "Facility Vacancy", "Distance from Air", "Distance from Sea", "Distance from Land");
            Console.WriteLine("-------------------------------------------------------------------------------------------------------------------");
            foreach (SHNFacility shn in shnList)
            {
                Console.WriteLine("{0, -18} {1, -18} {2, -18} {3, -18} {4, -18} {5}",
                    shn.FacilityName, shn.FacilityCapacity, shn.FacilityVacancy, shn.DistFromAirCheckpoint, shn.DistFromSeaCheckpoint, shn.DistFromLandCheckpoint);
            }
        }
        static void DisplaySafeEntry(Person p)
        {
            int i = 1;
            Console.WriteLine("{0, 45}", "Safe Entry Record");
            Console.WriteLine("-------------------------------------------------------------------------------------");
            Console.WriteLine("{0, -5} {1, -25} {2, -25} {3}",
                "#", "Check In", "Check Out", "Location");
            Console.WriteLine("-------------------------------------------------------------------------------------");
            foreach (SafeEntry se in p.SafeEntryList)
            {
                if(se.CheckOut == DateTime.MinValue)
                {
                    Console.WriteLine("{0, -5} {1, -25} {2, -25} {3}",
                        i, se.CheckIn, "-", se.Location.BusinessName);

                }
                else
                {
                    Console.WriteLine("{0, -5} {1, -25} {2, -25} {3}",
                        i, se.CheckIn, se.CheckOut, se.Location.BusinessName);
                }
                i += 1;
            }
        }
        static Resident SearchResident(List<Resident>rList,string name)
        {
            foreach(Resident r in rList)
            {
                if(r.Name==name)
                {
                    return r;
                }
            }
            return null;
        }
        static Visitor SearchVisitor(List<Visitor> vList, string name)
        {
            foreach(Visitor v in vList)
            {
                if(v.Name == name)
                {
                    return v;
                }
            }
            return null;
        }
        static BusinessLocation SearchBusinessLocation(List<BusinessLocation> blList, string bname)
        {
            foreach (BusinessLocation bl in blList)
            {
                if (bl.BusinessName == bname)
                {
                    return bl;
                }
            }
            return null;
        }
        static SafeEntry SearchSafeEntry(Person p1, int num)
        {
            if (p1.SafeEntryList.Count < (num - 1))
            {
                return null;
            }
            else
            {
                if(p1.SafeEntryList[num - 1].CheckOut == DateTime.MinValue)
                {
                    return p1.SafeEntryList[num - 1];
                }
                else
                {
                    return null;
                }
            }
        }
        static SHNFacility SearchSHNFacility(List<SHNFacility> shnList, string facilityName)
        {
            foreach(SHNFacility shn in shnList)
            {
                if(shn.FacilityName == facilityName)
                {
                    return shn;
                }
            }
            return null;
        }
        static Person SearchForPerson(List<Person> pList, string name)
        {
            foreach(Person p in pList)
            {
                if (p.Name == name)
                {
                    return p;
                }
            }
            return null;
        }  
        static Random Random = new Random(); //random number generator

        // Generates a random number within a range.      
        static int RandomNumber(int min, int max)
        {
            return Random.Next(min, max);
        }
        static string SerialNumberGenerator(List<TraceTogetherToken> tttList)
        {
            while(true)
            {
                var serialnum = new StringBuilder();
                serialnum.Append(Program.RandomNumber(10000, 99999));
                foreach(TraceTogetherToken ttt in tttList)
                {
                    if(Convert.ToString(serialnum)==ttt.SerialNo)
                    {
                        continue;
                    }
                    else
                    {
                        return serialnum.ToString();
                    }
                }
            }
        }
        static void InitCCList(List<string> collectionList)
        {
            collectionList.Add("Canberra CC");
            collectionList.Add("Bukit CC");
            collectionList.Add("Commonwealth CC");
            collectionList.Add("Admiralty CC");
            collectionList.Add("Dunearn CC");
            collectionList.Add("Khatib CC");
            collectionList.Add("Jurong CC");
            collectionList.Add("Changi CC");
            collectionList.Add("Bedok CC");
            collectionList.Add("Serangoon CC");
            collectionList.Add("Telok Blangah CC");
            collectionList.Add("Punggol CC");
            collectionList.Add("Aljunied CC");
            collectionList.Add("Farrer Park CC");
            collectionList.Add("Tiong Bahru CC");
            collectionList.Add("Toa Payoh CC");
        }
        static string AssignCollectionPoint(List<string>collectionList)
        {
            int index = RandomNumber(0, collectionList.Count - 1);
            return collectionList[index];
        }
    }
    //token doesnt show
    //vacancies
    //shn charges for newly created person
    //alice not working

}

