﻿//============================================================
// Student Number : S0194152D, S10205563J
// Student Name : Tan Jun Jie, Amos Yeong
// Module Group : T01
//============================================================

using System;
using System.Collections.Generic;
using System.Text;

namespace PRG2_T01_Team1
{
    abstract class Person
    {
        public string Name { get; set; }

        public List<SafeEntry> SafeEntryList { get; set;} = new List<SafeEntry>();

        public List<TravelEntry> TravelEntryList { get; set; } = new List<TravelEntry>();

        public Person() { }
        public Person(string name)
        {
            Name = name;
        }
        public void AddTravelEntry(TravelEntry travelEntry)
        {
            TravelEntryList.Add(travelEntry);
        }
        public void AddSafeEntry(SafeEntry safeEntry)
        {
            SafeEntryList.Add(safeEntry);
        }
        public abstract double CalculateSHNCharges();

        public override string ToString()
        {
            //testing out something here
            string safeEntryInfoList = "";
            string travelEntryInfoList = "";
            foreach(SafeEntry se in SafeEntryList)
            {
                safeEntryInfoList += se.ToString() + "\n";
            }
            foreach(TravelEntry te in TravelEntryList)
            {
                travelEntryInfoList += te.ToString() + "\n";
            }
            return "Name: " + Name + "\tSafe Entry: " + 
                
                safeEntryInfoList + "\tTravel Entry: " + travelEntryInfoList;
        }
    }
}
