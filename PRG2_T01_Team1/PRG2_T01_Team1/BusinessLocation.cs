﻿//============================================================
// Student Number : S0194152D, S10205563J
// Student Name : Tan Jun Jie, Amos Yeong
// Module Group : T01
//============================================================

using System;
using System.Collections.Generic;
using System.Text;

namespace PRG2_T01_Team1
{
    class BusinessLocation
    {
        public string BusinessName { get; set; }
        public string BranchCode { get; set; }
        public int MaximumCapacity { get; set; }
        public int VisitorsNow { get; set; }
        public BusinessLocation() { }
        public BusinessLocation(string businessName,string branchCode,int maxCap)
        {
            BusinessName = businessName;
            BranchCode = branchCode;
            MaximumCapacity = maxCap;
        }
        public bool IfFull() 
        {
            return true;
        }
        public override string ToString()
        {
            return "Business Name: " + BusinessName + "Business Location: " + BranchCode
                + "Maximum Capacity: " + MaximumCapacity;
        }
    }
}
