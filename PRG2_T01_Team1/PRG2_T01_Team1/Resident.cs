﻿//============================================================
// Student Number : S0194152D, S10205563J
// Student Name : Tan Jun Jie, Amos Yeong
// Module Group : T01
//============================================================

using System;
using System.Collections.Generic;
using System.Text;

namespace PRG2_T01_Team1
{
    class Resident: Person
    {
        public string Address { get; set; }
        public DateTime LastLeftCountry { get; set; }
        public TraceTogetherToken Token { get; set; }

        public Resident(string name, string address, DateTime llc)
        {
            Name = name;
            Address = address;
            LastLeftCountry = llc;
        }
        public override double CalculateSHNCharges()
        {
            foreach (TravelEntry te in TravelEntryList)
            {
                if (te.LastCountryOfEmbarkation == "New Zealand" || te.LastCountryOfEmbarkation == "Vietnam")
                {
                    return 200;
                }
                else if (te.LastCountryOfEmbarkation == "Macao SAR")
                {
                    return 220;
                }
                else
                {
                    return 1220;
                }
            }
            return 0;
        }
        public override string ToString()
        {
            return base.ToString() + "\nAddress: " + Address + "\nLast Left Country: " + LastLeftCountry + "\nToken: " + Token;
        }
    }
}
