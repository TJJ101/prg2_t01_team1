﻿//============================================================
// Student Number : S0194152D, S10205563J
// Student Name : Tan Jun Jie, Amos Yeong
// Module Group : T01
//============================================================


using System;
using System.Collections.Generic;
using System.Text;

namespace PRG2_T01_Team1
{
    class TraceTogetherToken
    {
        public string SerialNo { get; set; }
        public string CollectionLocation { get; set; }
        public DateTime ExpiryDate { get; set; }

        public TraceTogetherToken() { }

        public TraceTogetherToken(string sNo, string cLoc, DateTime eDate)
        {
            SerialNo = sNo;
            CollectionLocation = cLoc;
            ExpiryDate = eDate;
        }

        public bool IsEligibleForReplacement()
        {
            if (((ExpiryDate.Year - DateTime.Now.Year == 0) && (ExpiryDate.Month - DateTime.Now.Month == 1))
            || ((DateTime.Now.Month - ExpiryDate.Month >= 0) && (DateTime.Now.Year - ExpiryDate.Year>= 0)))
            {
                return true;
            }
            else
            {
                return false;
            }
                
        }
        public void ReplaceToken(string sNo, string cLoc)
        {
            SerialNo = sNo;
            CollectionLocation = cLoc;
            ExpiryDate = DateTime.Now.AddMonths(6);

        }
    }
}
