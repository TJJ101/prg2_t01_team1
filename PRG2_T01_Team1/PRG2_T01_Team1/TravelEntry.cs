﻿//============================================================
// Student Number : S0194152D, S10205563J
// Student Name : Tan Jun Jie, Amos Yeong
// Module Group : T01
//============================================================


using System;
using System.Collections.Generic;
using System.Text;

namespace PRG2_T01_Team1
{
    class TravelEntry
    {
        public string LastCountryOfEmbarkation { get; set; }
        public string EntryMode { get; set; }
        public DateTime EntryDate { get; set; }
        public DateTime ShnEndDate { get; set; }
        public SHNFacility ShnStay { get; set; }
        public bool IsPaid { get; set; }
        public TravelEntry() { }
        public TravelEntry(string LCOE,string entMode, DateTime entDate)
        {
            LastCountryOfEmbarkation = LCOE;
            EntryMode = entMode;
            EntryDate = entDate;
        }
        public void AssignSHNFacility(SHNFacility facility)
        {
            ShnStay = facility;
        }
        public void CalculateSHNDuration()
        {
            if (LastCountryOfEmbarkation=="New Zealand"|| LastCountryOfEmbarkation=="Vietnam")
            {
                ShnEndDate = EntryDate;
            }
            else if (LastCountryOfEmbarkation == "Macao SAR")
            {
                ShnEndDate = EntryDate.AddDays(7);
            }
            else
            {
                ShnEndDate = EntryDate.AddDays(14);
            }
        }
        public override string ToString()
        {
            return "Last Country of Embarkation: " + LastCountryOfEmbarkation
                + "\tEntry Mode: " + EntryMode
                + "\tEntry Date: " + EntryDate
                + "\tSHN End Date: " + ShnEndDate
                + "\tSHN Stay: " + ShnStay;
        }
    }
}
