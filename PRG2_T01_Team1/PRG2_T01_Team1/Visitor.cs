﻿//============================================================
// Student Number : S0194152D, S10205563J
// Student Name : Tan Jun Jie, Amos Yeong
// Module Group : T01
//============================================================

using System;
using System.Collections.Generic;
using System.Text;

namespace PRG2_T01_Team1
{
    class Visitor: Person
    {
        public string PassportNo { get; set; }
        public string Nationality { get; set; }

        public Visitor(string name, string passNo, string nation) : base(name)
        {
            PassportNo = passNo;
            Nationality = nation;
        }

        public override double CalculateSHNCharges()
        {
            foreach(TravelEntry te in TravelEntryList)
            {
                if(te.LastCountryOfEmbarkation == "New Zealand" || te.LastCountryOfEmbarkation == "Vietnam" || te.LastCountryOfEmbarkation == "Macao SAR")
                {
                    return 280;
                }
                else
                {
                    return 2200; 
                }
            }
            return 0;
        }
        public override string ToString()
        {
            return base.ToString() + "\tPassport No: " + PassportNo + "\tNationality: " + Nationality;
        }
    }
}
