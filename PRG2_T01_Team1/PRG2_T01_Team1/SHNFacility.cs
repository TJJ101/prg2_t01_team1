﻿//============================================================
// Student Number : S0194152D, S10205563J
// Student Name : Tan Jun Jie, Amos Yeong
// Module Group : T01
//============================================================


using System;
using System.Collections.Generic;
using System.Text;

namespace PRG2_T01_Team1
{
    class SHNFacility
    {
        public string FacilityName { get; set; }
        public int FacilityCapacity { get; set; }
        public int FacilityVacancy { get; set; }
        public double DistFromAirCheckpoint { get; set; }
        public double DistFromSeaCheckpoint { get; set; }
        public double DistFromLandCheckpoint { get; set; }

        public SHNFacility() { }
        public SHNFacility(string facilName,int facilCap,double DistFromAir, double DistFromSea, double DistFromLand)
        {
            FacilityName = facilName;
            FacilityCapacity = facilCap;
            DistFromAirCheckpoint = DistFromAir;
            DistFromSeaCheckpoint = DistFromSea;
            DistFromLandCheckpoint = DistFromLand;
        }
        public double CalculateTravelCost(string entryMode, DateTime entryDateTime)
        {
            if (((6 <= entryDateTime.Hour) && (entryDateTime.Hour < 9)) || ((18 <= entryDateTime.Hour) && (entryDateTime.Hour < 00)))
            {
                if (entryMode == "Air")
                {
                    return 1.25 * (50 + DistFromAirCheckpoint * 0.22);
                }
                else if (entryMode == "Land")
                {
                    return 1.25 * (50 + DistFromLandCheckpoint * 0.22);
                }
                else if (entryMode == "Sea")
                {
                    return 1.25 * (50 + DistFromSeaCheckpoint * 0.22);
                }
            }
            else if ((entryDateTime.Hour < 6) || (entryDateTime.Hour >= 0))
            {
                if (entryMode == "Air")
                {
                    return  1.50 * (50 + DistFromAirCheckpoint * 0.22);
                }
                else if (entryMode == "Land")
                {
                    return 1.50 * (50 + DistFromLandCheckpoint * 0.22);
                }
                else if (entryMode == "Sea")
                {
                    return 1.50 * (50 + DistFromSeaCheckpoint * 0.22);
                }
            }
            return 0;
        }
        public bool IsAvailable()
        {
            return false;
        }
        public override string ToString()
        {
            return "Facility Name: " + FacilityName + "\tFacility Capacity: " + FacilityCapacity
                + "\tFacility Vacancy: " + FacilityVacancy + "\tDistance from Air Checkpoint: " + DistFromAirCheckpoint
                + "\tDistance From Sea Checkpoint: " + DistFromSeaCheckpoint + "\tDistance from Land Checkpoint: " 
                + DistFromAirCheckpoint;
        }
    }
}
