﻿//============================================================
// Student Number : S0194152D, S10205563J
// Student Name : Tan Jun Jie, Amos Yeong
// Module Group : T01
//============================================================

using System;
using System.Collections.Generic;
using System.Text;

namespace PRG2_T01_Team1
{
    class SafeEntry
    {
        public DateTime CheckIn { get; set; }
        public DateTime CheckOut { get; set; }
        public BusinessLocation Location { get; set; }
        public SafeEntry() { }
        public SafeEntry(DateTime checkIn,BusinessLocation location)
        {
            CheckIn = checkIn;
            Location = location;
        }
        public void PerformCheckOut()
        {
            CheckOut = DateTime.Now;
        }
        public override string ToString()
        {
            return CheckIn + "," + CheckOut + "," + Location;  
        }

    }
}
